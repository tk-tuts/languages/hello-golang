package main

import (
	"fmt"
	"os"
	"github.com/Sirupsen/logrus"
)

func main() {
	fmt.Println("Go Logrus")
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetOutput(os.Stdout)

	logrus.Println("Now with logrus")

	logrus.SetFormatter(&logrus.JSONFormatter{})

	logrus.WithFields(logrus.Fields{
		"animal": "walrus",
		"size": 10,
	}).Info("A group of walrus")
}
