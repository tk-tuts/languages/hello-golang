package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"flag"
	"github.com/Jeffail/gabs"
)

var (
	env = flag.String("env", "local", "Environment")
)

var jsonConfig = "{\"a\":\"test\",\"c\":{\"c1\":2}}"

func loadConfig(env string) {
	viper.SetConfigName(env)
	err := viper.ReadInConfig()
	if err != nil {
		logrus.WithError(err).WithField("env", env).Panic("config: error reading config")
	}
}

func init() {
	logrus.Warnln("Init is called")
	viper.AddConfigPath("./go-libraries/test-viper/config")
	viper.SetConfigType("yaml")
}

func main() {
	logrus.Infoln("Go Viper")
	loadConfig(*env);

	logrus.Errorln(viper.GetString("app.name"))

	logrus.SetLevel(logrus.DebugLevel)
	logrus.Debugln(viper.GetFloat64("app.version"))

	// set config
	jsonParsed, err := gabs.ParseJSON([]byte(jsonConfig))
	if err == nil {
		logrus.Infoln(jsonParsed)

		a := jsonParsed.S("a").Data().(string)
		c := jsonParsed.S("c", "c1").Data().(float64)
		logrus.Infoln(a)
		logrus.Infoln(c)

		viper.Set("a", a)
		viper.Set("c.c1", c)

		logrus.Infoln(viper.GetString("a"))
		logrus.Infoln(viper.GetInt("c.c1"))
	} else {
		logrus.Errorln(err)
	}
}
