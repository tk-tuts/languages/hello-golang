package main

import (
	"fmt"
	"runtime"
	"time"
)

func main() {
	defer fmt.Println("The END")

	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}

	switchFunc()
}

func switchFunc() {
	fmt.Print("Go runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		fmt.Printf("%s", os)
	}

	fmt.Println();

	// switch without condition
	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Good morning!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon.")
	default:
		fmt.Println("Good evening.")
	}
}
