package main

import "fmt"

type Vertex struct {
	X int
	Y int
}

func main() {
	v := Vertex{1, 2}
	v.X = 4
	fmt.Println(v)

	// implicit Y = 0
	v2 := Vertex{X: 3}
	fmt.Println(v2)

	// arrays
	var a [3]string
	a[0] = "Hello"
	a[1] = "World"
	fmt.Println(cap(a), a)

	// direct init
	primes := [2]int{2, 3}
	fmt.Println(primes)

	// struct array
	s := []Vertex{
		{1, 2},
		{2, 3},
	}
	fmt.Println(s)
	fmt.Println(len(s))

	// appending
	var b []string
	b = append(b, "!")
	fmt.Println(b)

	// range
	var pow = []int{1, 2, 4}
	for i, v := range pow {
		fmt.Println(i, v, )
	}

	// declare map
	var m map[string]Vertex

	// init map
	m = make(map[string]Vertex)
	m["test"] = Vertex{3,4}
	fmt.Println(m)

	var n = map[string]Vertex{
		"test": Vertex{1,2},
		"test2": Vertex{2,3},
	}
	fmt.Println(n)

	o := make(map[string]Vertex)
	o["test"] = Vertex{3,4}
	fmt.Println(o)

	elem, ok := m["test"]
	if ok{
		fmt.Println(elem)
	}

	// function values
	add := func(x, y int) int{
		return x + y
	}
	fmt.Println("direct call", add(3, 4))
	fmt.Println("func call", compute(add))
}

func compute(fn func(int, int) int) int{
	return fn(3, 4)
}