package main

import "fmt"
import "gitlab.com/tk-tuts/hello-golang/go-tour/09_concurrency/con1"

func main(){
	fmt.Println("Concurrency continued")

	con1.RunFibonacci()
	con1.RunFibonacci2()
	con1.RunMutex()
}
