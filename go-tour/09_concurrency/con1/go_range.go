package con1

import "fmt"

func fibonacci(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		c <- x
		x, y = y, x + y
	}
	close(c)
}

func fibonacci2(c, quit chan int) {
	x, y := 0, 1
	for {
		select {
		case c <- x:
			x, y = y, x + y

		case <-quit:
			fmt.Println("quit")
			return
		}
	}
}

func RunFibonacci() {
	ch := make(chan int, 10)
	fibonacci(cap(ch), ch)

	for i := range ch {
		fmt.Println(i)
	}
}

func RunFibonacci2(){
	c := make(chan int)
	quit := make(chan int)

	go func(){
		for i:=0; i < 10; i++{
			fmt.Println(<- c)
		}
		quit <- 0
	}()
	fibonacci2(c, quit)
}
