package main

import "fmt"

type Person interface {
	Fullname() string
}

type PersonPtr interface {
	FullnamePtr() string
}

func main(){
	var a Person
	s := Student{"Keerthikan", "Thurairatnam"}
	a = s
	fmt.Println("Student ", a.Fullname())

	var s2 *Student
	var b PersonPtr
	b = s2
	fmt.Println(b.FullnamePtr())

	// Type assertion
	var i interface {} = s

	t := i.(Person)
	fmt.Println(t)

	t2, ok := i.(PersonPtr)
	fmt.Println(t2, ok)

	if err := throwStdErr(); err != nil {
		fmt.Println(err)
	}
}


type Student struct{
	firstname string
	lastname string
}

func (s Student) Fullname() string{
	return s.firstname + " " + s.lastname
}

func (s *Student) FullnamePtr() string{
	if s == nil {
		return "<nil>"
	}
	return s.firstname + " " + s.lastname
}

func (s Student) String() string{
	return fmt.Sprintf("f: %s, l: %s", s.firstname, s.lastname)
}

func throwStdErr() error{
	return &StudentError{
		"useless error message",
	}
}

type StudentError struct{
	What string
}

func (s *StudentError) Error() string{
	return fmt.Sprintf("Error: something went wrong with %s", s.What)
}


