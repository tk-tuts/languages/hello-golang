package main

import (
	"fmt"
)

type Person struct {
	name string
	age  int
}

func (p Person) GetDetail() string {
	return fmt.Sprintf("%s - %d", p.name, p.age)
}

func GetDetail(p Person) string{
	return fmt.Sprintf("%s - %d", p.name, p.age)
}

func (p Person) SetAge(age int){
	p.age = age
}

func (p *Person) SetAgeByPointer(age int){
	p.age = age
}

func main() {
	fmt.Println("Methods")

	p := Person{name: "Keerthikan", age: 25}
	fmt.Println(p.GetDetail())
	fmt.Println(GetDetail(p))

	// structs are pass by value
	p.SetAge(26)
	fmt.Println(p.GetDetail())

	ptr := &p
	ptr.SetAge(28)
	fmt.Println(ptr.GetDetail())

	// pass by reference
	p.SetAgeByPointer(27)
	fmt.Println(p.GetDetail())
}


