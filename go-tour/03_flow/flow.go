package main

import (
	"fmt"
	"math"
)

func main() {
	forFunc()
	forWhile()
	ifFunc(-3)
	ifFunc(3)
}

func ifFunc(x int){
	if x < 0{
		fmt.Println("minus")
	}else {
		fmt.Println("plus")
	}

	// short statement will always executed
	if y := math.Sqrt(float64(x)); x >= 0 {
		fmt.Println(x, y)
	}else{
		fmt.Println(x, y)
	}
}

func forWhile(){
	sum := 1
	for sum < 1000 {
		sum += sum
	}
	fmt.Println(sum)
}

func forFunc(){
	sum := 0
	for i := 0; i < 10; i++ {
		sum += i
	}
	fmt.Println(sum)

	sum = 1
	for ; sum < 1000;{
		sum += sum
	}
	fmt.Println(sum)
}
