package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

func main() {
	fmt.Println("\nGetting Started")
	fmt.Println("-----------------")

	fmt.Printf("hello, world\n")
	fmt.Println("The time is", time.Now())
	fmt.Println("My favorite number is", rand.Intn(10))

	fmt.Printf("Now you have %g problems\n", math.Sqrt(7))

	// exported variables
	fmt.Printf("PI: %g\n\n", math.Pi)

	// functions
	fmt.Println("Functions")
	fmt.Println("-----------------")

	fmt.Printf("4+3=%d\n", add(4, 3))
	fmt.Printf("4-3=%d\n", substract(4, 3))

	a, b := swap(4, 3)
	fmt.Printf("4 and 3 will %d, %d\n", a, b)
	fmt.Println(split(17))
}

func add(x int, y int) int {
	return x + y
}

// omit shared types
func substract(x, y int) int {
	return x - y
}

// multiple results
func swap(x, y int) (int, int) {
	return y, x
}

// named return values
func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - 4
	return
}
